> cd /var/lib/libvirt/images/
>
> wget https://cloud.centos.org/centos/8/x86_64/images/CentOS-8-GenericCloud-8.3.2011-20201204.2.x86_64.qcow2
>
> qemu-img create -f qcow2 -b /var/lib/libvirt/images/CentOS-8-GenericCloud-8.3.2011-20201204.2.x86_64.qcow2 /var/lib/libvirt/images/provisioner.qcow2 120G
>
> virt-resize --expand /dev/vda1 /var/lib/libvirt/images/CentOS-8-GenericCloud-8.3.2011-20201204.2.x86_64.qcow2 /var/lib/libvirt/images/provisioner.qcow2
>
> virt-customize -a /var/lib/libvirt/images/provisioner.qcow2 --root-password password:Redhat01 --uninstall cloud-init --hostname provisioner.ocp4.example.local --run-command 'yum update -y'
>
> virt-customize -a /var/lib/libvirt/images/provisioner.qcow2 --selinux-relabel
>
> virt-install -q -n provisioner --vcpus 8 -r 16384 --os-type linux --os-variant rhel8.0 --disk path=/var/lib/libvirt/images/provisioner.qcow2,bus=scsi,size=120 --network bridge=provisioning,model=virtio,mac=52:54:00:67:2c:16 --network bridge=baremetal,model=virtio,mac=52:54:00:0d:7b:0c --cpu host-passthrough --graphics vnc --console pty,target_type=serial --noautoconsole --import
